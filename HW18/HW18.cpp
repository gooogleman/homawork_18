﻿// HW18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class AddPlayer
{
private:

	char name[256];
	int score = 0;

public:

	int GetScore()
	{
		return score;
	}
	
	void Enter()
	{
		std::cout << "Enter your name:";
		std::cin >> name;
		std::cout << "Enter score:";
		std::cin >> score;
		
	}
	
	void Show()
	{
		std::cout << name << " " << score << '\n';
	}
};

void AscendingOrderSort(AddPlayer* Array, int ArrayLength)
{
	AddPlayer Temporary;
	bool sort = true;

	while (sort)
	{
		sort = false;
		for (int i = 0; i < ArrayLength; i++)
		{
			if (Array[i].GetScore() < Array[i - 1].GetScore())
			{
				Temporary = Array[i];
				Array[i] = Array[i - 1];
				Array[i - 1] = Temporary;
				sort = true;
			}
		}
	}
}


int main()
{
	int size;
	std::cout << "Enter number of players: ";
	std::cin >> size;

	AddPlayer* Array = new AddPlayer[size];
	
	for (int i = 0; i < size; ++i)
	{
		Array[i].Enter();
	}
	std::cout << "Entered Massive:";
	std::cout << '\n';

	for (int i = 0; i < size; ++i)
	{	
		Array[i].Show();
		AscendingOrderSort(Array, size);
	}
	
	std::cout << std::endl;
	std::cout << "Sorted Massive:" << '\n';

	for (int i = 0; i < size; ++i)
	{
		Array[i].Show();
	}

	std::cout << "\n";
	
	delete[] Array;
	Array = nullptr;

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
